//always keep cursor on input
document.getElementById('inputTxt').addEventListener('blur',keepFocus,true);
//once submitted, do stuff
// document.getElementById('adminForm').addEventListener('submit',getInp);
getInp(document.getElementById('adminForm'));

function keepFocus() {
	this.focus();
}

function getInp(form) {
	var ctr = revctr = 0;
	form.addEventListener('submit',checkInp);
	form.addEventListener('keydown',previousInp);
	
function checkInp(e) {
	//prevent page load
	e.preventDefault();
	form = this;
	input = document.getElementById('inputTxt').value;
	//if value is clear
	if (input == "clear") { clearScr(form); }
		else {
			if (input == "help") {
				showHelp(form);
			}
			else if (input == "all customers") {
				showCustomers(form);
			}
			else if (input == "products") {
				modalProducts();
			}
			confirmInput(form);
		}
}

	function confirmInput(form) {
		//create new input
		var formGroup = document.getElementsByClassName('form-group')[ctr];
		//set to true to copy childnodes
		var newInpArea = formGroup.cloneNode(true);
		form.appendChild(newInpArea);
		//access input element, remove id and autofocus
		var inpArea = formGroup.firstElementChild.firstElementChild;
		inpArea.removeAttribute('id');
		inpArea.removeAttribute('autofocus');
		inpArea.setAttribute('disabled','');
		newInpArea.firstElementChild.firstElementChild.value = "";
		//do this only if no modal window is open
		var modalWindow = document.getElementById('modalProductsWindow');
		if(modalWindow.style.display == "" || modalWindow.style.display == "none")
		{
			setTimeout(function() {
				document.getElementById('inputTxt').focus();
			}, 10);
			document.getElementById('inputTxt').addEventListener('blur',keepFocus,true);
		}
		ctr++;
	}

	function clearScr(form) {
		//create new input by cloning before clearing content
		var formGroup = document.getElementsByClassName('form-group')[ctr];
		//reset ctr
		ctr = 0;
		//set to true to copy childnodes
		var newInpArea = formGroup.cloneNode(true);
		//clone that button too
		var formBtn = document.getElementById('termBtn').cloneNode(true);
		//clear content
			//remove intro text "Hello, Admin" during first use of clear
			var intro = document.getElementById('introTxt');
			if (intro) {
				var container = document.getElementById('adminTerminal');
				container.removeChild(intro);
			}
		while(form.hasChildNodes()) {
			form.removeChild(form.lastChild);
		}
		//append once cleared
		form.appendChild(newInpArea);
		form.appendChild(formBtn);
		//reset values and focus event
		document.getElementById('inputTxt').value = "";	
		document.getElementById('inputTxt').focus();
		document.getElementById('inputTxt').addEventListener('blur',keepFocus,true);
	}

	function showHelp(form) {
		var helplist = document.createElement("UL");
		var commands = ["<help> - this help command.",
						"<clear> - clear the screen.",
						"<products> - opens the products database window.",
						"<all customers> - shows a list of customers."];
		//loop through commands, insert into ul
		var l = commands.length, i = 0;
		for(;i<l;i++){
			var li = document.createElement("LI");
			var textnode = document.createTextNode(commands[i]);
			li.appendChild(textnode);
			helplist.appendChild(li);
		}
		//append command ul to form
		form.appendChild(helplist);
	}

	function showCustomers(form) {
		//make an ajax request to get data from bibliophile(users) table
		var xmlhttp = new XMLHttpRequest();
	        xmlhttp.onreadystatechange = function() {
	            if (this.readyState == 4 && this.status == 200) {
	            	var customersDiv = document.createElement("DIV");
	            	customersDiv.innerHTML = this.responseText;
	            	//insert before for better ui, ajax goes before new input
	                form.insertBefore(customersDiv, form.lastElementChild);
	            }
	        };
	        xmlhttp.open("POST", "showCustomers.php");
	        xmlhttp.send();
	}

	function modalProducts() {
		//remove focus from input text
		document.getElementById('inputTxt').removeEventListener('blur',keepFocus,true);
		document.getElementById('inputTxt').blur();
		var modalProducts = document.getElementById('modalProductsWindow');
		modalProducts.style.display = "block";
		//hide main window scrollbar
		document.body.style.overflow = "hidden";
		//if any of the close buttons are pressed
		document.getElementsByClassName('closeModal')[0].addEventListener('click',function(){
			//hide modal window
			modalProducts.style.display = "none";
			//close any opened products view
			hideOpenedOptions();
			//refocus to input text
			setTimeout(function() {
				document.getElementById('inputTxt').focus();
			}, 10);
			document.getElementById('inputTxt').addEventListener('blur',keepFocus,true);
			//return scrollbar of main body
			document.body.style.overflow = "visible";
		});
		document.getElementById('viewProds').addEventListener('click', function(e){
			//hide any opened products options
			hideOpenedOptions();
			//stop immediate propagation to prevent multiple ajax requests
			e.stopImmediatePropagation();
			//ajax request to append results to modal content
			var xmlhttp = new XMLHttpRequest();
		        xmlhttp.onreadystatechange = function() {
		            if (this.readyState == 4 && this.status == 200) {
		            	var productsDiv = document.createElement("DIV");
		            	productsDiv.innerHTML = this.responseText;
		            	productsDiv.setAttribute('id','allProdsView');
		            	productsDiv.classList.add("fadeInDown","animated","table-responsive");
		            	var modalContent = document.getElementById('productsOptionsView');
		                modalContent.appendChild(productsDiv);
		                //loop through editProd class to add event listener editProduct function
		                var editBtn = document.getElementsByClassName('editProd');
		                for(var i = 0, l = editBtn.length; i<l; i++)
		                	editBtn[i].addEventListener('click',editProductFnc);
		            }
		        };
		        xmlhttp.open("POST", "showBooks.php");
		        xmlhttp.send();
		});
		document.getElementById('addProds').addEventListener('click', function(){
			//hide any opened products options
			hideOpenedOptions();
			//reset form values upon adding another book
			var addProductsForm = document.getElementById('addProductsForm');
			addProductsForm.reset();
			document.getElementById('addProductsFormCont').style.display = "flex";
			//set focus to Title input
			document.querySelector("input[name='title']").focus();
			addProductsForm.addEventListener('submit',function(e){
				//don't let the page load
				e.preventDefault();
				//stop immediate propagation to prevent multiple form submissions
				e.stopImmediatePropagation();
				//ajax request to add book and echo success message to a div
				//get the values
				var title = document.querySelector("input[name='title']").value,
					//use files[0].name instead of value to prevent getting fakepath, we only need the img name
					image = document.querySelector("input[name='image']").files[0].name,
					author = document.querySelector("input[name='author']").value,
					publisher = document.querySelector("input[name='publisher']").value,
					genre = document.querySelector("select[name='genre']").value,
					bookdesc = document.querySelector("textarea[name='bookdesc']").value,
					stocks = document.querySelector("input[name='stocks']").value,
					price = document.querySelector("input[name='price']").value,
					newBook = JSON.stringify({"title":title,"image":image,"author":author,"publisher":publisher,"genre":genre,"bookdesc":bookdesc,
											  "stocks":stocks,"price":price}),
					xmlhttp = new XMLHttpRequest();
			        xmlhttp.onreadystatechange = function() {
			            if (this.readyState == 4 && this.status == 200) {
			            	//hide the form on success
			            	hideOpenedOptions();
			            	var addedBook = document.createElement("H3");
			            	addedBook.innerHTML = this.responseText;
			            	addedBook.setAttribute('id','addedBookMsg');
			            	addedBook.classList.add("flash","animated");
			            	var modalContent = document.getElementById('productsOptionsView');
			                modalContent.appendChild(addedBook);
			            }
			        };
			        xmlhttp.open("POST", "addBook.php");
			        xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
			        xmlhttp.send(newBook);
			});
		});

		function hideOpenedOptions(mode = 0) {
			//mode depends on the editbook option to not remove products
			var optionsView = document.getElementById('productsOptionsView'),
				l = optionsView.children.length;
			for(var i = 0; i<l; i++) {
				var openedOptions = document.getElementById(optionsView.children[i].id);
				if(mode == 0 || openedOptions.id == 'editProductsFormCont') {openedOptions.style.display = "none";}
					//to avoid repetition of view products
					if((mode==0 && openedOptions.id=='allProdsView') || openedOptions.id=='addedBookMsg' || openedOptions.id=='editedBookMsg' || openedOptions.id=='removedBookMsg' || (mode==0 && openedOptions.id=='editFormHR') || openedOptions.id=='editBookSelection') 
						{
							openedOptions.parentNode.removeChild(openedOptions);
							//reduce i & l
							i--;
							l--;
						}
			}
		}

		function editProductFnc(e) {
			//clean up, send checker into hideOpenedOptions to ignore allProdsView
			hideOpenedOptions(1);
			//get the id of (grand)parent node of button (parent node == td, grandparent node == tr)
			var bookRow = document.getElementById(this.parentNode.parentNode.id);
			//created a clone of all the products
			var	allBooks = bookRow.parentNode.cloneNode(true),
				currBkIndx = Array.prototype.indexOf.call(bookRow.parentNode.children, bookRow),
				bookCount = allBooks.children.length; //length value to check limits of book selection button
			//after cloning, loop through siblings removing each next element
			while(bookRow.nextElementSibling) {
				var nextBookRow = bookRow.nextElementSibling;
				bookRow.parentNode.removeChild(nextBookRow);
			}
			//loop through each sibling removing each previous element
			while(bookRow.previousElementSibling) {
			    var prevBookRow = bookRow.previousElementSibling;
			    bookRow.parentNode.removeChild(prevBookRow);
			}
			//create a div to go to next element
			var editBtnDiv = document.createElement("DIV"),
				nextEditBtn = document.createElement("BUTTON"),
				prevEditBtn = document.createElement("BUTTON"),
				productsOptionsView = document.getElementById('productsOptionsView');
			//add attributes to the buttons
			nextEditBtn.setAttribute("id","nextBook");
			nextEditBtn.classList.add("btn","btn-primary");
			nextEditBtn.innerHTML = "Next Book &rarr;";
			//check position for disabling buttons
			if(currBkIndx == bookCount-1) { 
				nextEditBtn.classList.add("disabled"); 
				nextEditBtn.setAttribute("aria-disabled","true"); 
			} 
			prevEditBtn.setAttribute("id","prevBook");	
			prevEditBtn.classList.add("btn","btn-primary");
			prevEditBtn.innerHTML = "&larr; Prev Book";
			//check position for disabling buttons
			if(currBkIndx == 0) { 
				prevEditBtn.classList.add("disabled"); 
				prevEditBtn.setAttribute("aria-disabled","true"); 
			} 
			//append buttons to the div container
			editBtnDiv.appendChild(nextEditBtn);
			editBtnDiv.appendChild(prevEditBtn);
			editBtnDiv.setAttribute("id","editBookSelection");
			productsOptionsView.appendChild(editBtnDiv);
			//event listener for next & previous book buttons
			nextEditBtn.addEventListener('click',showNextBook);
			prevEditBtn.addEventListener('click',showPrevBook);
			function showNextBook() {
				//dynamically get value of 
				//each book table row has an id of book1 but children use standard index numbering, starts with 0, 
				//using bookID ends up getting the nextElementSibling
					nextBookIDN = allBooks.children[++currBkIndx].firstElementChild.innerHTML;	
				updateBookTable(nextBookIDN);
				//only update as long as there is still a next book 
				if(currBkIndx+1 == bookCount) { 
					nextEditBtn.classList.add("disabled"); 
					nextEditBtn.setAttribute("aria-disabled","true"); 
					nextEditBtn.removeEventListener("click",showNextBook);
				} 
				//enable previous button
				else if (prevEditBtn.classList.contains("disabled")) {
					prevEditBtn.classList.remove("disabled");
					prevEditBtn.removeAttribute("aria-disabled");
					prevEditBtn.addEventListener('click',showPrevBook);
				}
			}
			function showPrevBook() {
				//each book table row has an id of book1 but children use standard index numbering, starts with 0, 
				//using bookID-2 ends up getting the prevElementSibling
					prevBookIDN = allBooks.children[--currBkIndx].firstElementChild.innerHTML;
				updateBookTable(prevBookIDN);
				//only update as long as there is still a prev book
				if(currBkIndx == 0) { 
					prevEditBtn.classList.add("disabled"); 
					prevEditBtn.setAttribute("aria-disabled","true"); 
					prevEditBtn.removeEventListener("click",showPrevBook);
				}
				//enable next button
				else if (nextEditBtn.classList.contains("disabled")) { 
					nextEditBtn.classList.remove("disabled");
					nextEditBtn.removeAttribute("aria-disabled");
					nextEditBtn.addEventListener('click',showNextBook);
				}
			}
			//turn the edit button into a remove button
			var editButton = bookRow.lastElementChild.firstElementChild;
			//create a new button and remove edit button
			removeButton = document.createElement("BUTTON");
			removeButton.innerHTML = "Remove Book";
			removeButton.classList.add("btn","btn-danger");
			editButton.parentNode.appendChild(removeButton);
			editButton.parentNode.removeChild(editButton);
			removeButton.addEventListener("click", function(){
				document.getElementById('removeConfirmation').style.display = "block";
				document.getElementById('removeBookConfirmed').addEventListener("click",removeBook);
				document.getElementById('removeBookCancel').addEventListener("click",function(){
					document.getElementById('removeConfirmation').style.display = "none";
				});
			});
			function removeBook() {
	        	var xmlhttp = new XMLHttpRequest(),
        	        removeBookIDN = "bookIDN="+(Number(bookRow.children[0].innerHTML));
    	        xmlhttp.onreadystatechange = function() {
    	            if (this.readyState == 4 && this.status == 200) {
    	            	//hide the form on success except for the table
    	            	hideOpenedOptions();
    	            	var removedBook = document.createElement("H3");
    	            	removedBook.innerHTML = this.responseText;
    	            	removedBook.setAttribute('id','removedBookMsg');
    	            	removedBook.classList.add("flash","animated");
    	            	var modalContent = document.getElementById('productsOptionsView');
    	                modalContent.appendChild(removedBook);
    	            }
    	        };
    	        xmlhttp.open("POST", "removeBook.php");
    	        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    	        xmlhttp.send(removeBookIDN);
			}
			//append hr and edit products form to content container
			var editProductsFormCont = document.getElementById('editProductsFormCont'),
			    hrule = document.createElement("HR");
			hrule.setAttribute("id","editFormHR");
			productsOptionsView.appendChild(hrule);
			productsOptionsView.appendChild(editProductsFormCont);
			editProductsFormCont.style.display = "flex";
			//insert previous values into the edit form
			editFormGetValues();
			function editFormGetValues() {
				var title = document.querySelector("input[name='edittitle']"),
					author = document.querySelector("input[name='editauthor']"),
					publisher = document.querySelector("input[name='editpublisher']"),
					genre = document.querySelector("select[name='editgenre']"),
					bookdesc = document.querySelector("textarea[name='editbookdesc']"),
					stocks = document.querySelector("input[name='editstocks']"),
					price = document.querySelector("input[name='editprice']");
				//insert values from the bookRow table into the form
				title.value = bookRow.children[2].innerHTML;
				title.focus();
				//image value should be added into php automatically on ajax call if value isn't modified
				author.value = bookRow.children[3].innerHTML;
				publisher.value = bookRow.children[4].innerHTML;
				genre.value = bookRow.children[5].innerHTML;
				bookdesc.value = bookRow.children[6].innerHTML;
				stocks.value = bookRow.children[7].innerHTML;
				//conversion for price value
				var priceStr = String(bookRow.children[8].innerHTML);
				price.value = parseFloat(priceStr.slice(5,priceStr.length));
			}
			//event handler and ajax request for edit products form
			document.getElementById('editProductsForm').addEventListener('submit',function(e){
				//don't let the page load
				e.preventDefault();
				//stop immediate propagation to prevent multiple form submissions
				e.stopImmediatePropagation();
				//ajax request to edit book and echo success message to a div
				//get the values, including the bookIDN from the book table
				//check the value of files[0] if the file has been changed
				var imgFile = document.querySelector("input[name='editimage']"),
					image;
				if(imgFile.files[0] === undefined) {image = " ";}
					else {image = imgFile.files[0].name;}
				var bookIDN = Number(document.getElementsByTagName('td')[0].innerHTML),
					title = document.querySelector("input[name='edittitle']").value,
					author = document.querySelector("input[name='editauthor']").value,
					publisher = document.querySelector("input[name='editpublisher']").value,
					genre = document.querySelector("select[name='editgenre']").value,
					bookdesc = document.querySelector("textarea[name='editbookdesc']").value,
					stocks = document.querySelector("input[name='editstocks']").value,
					price = document.querySelector("input[name='editprice']").value,
					editBook = JSON.stringify({"bookIDN":bookIDN,"title":title,"image":image,"author":author,"publisher":publisher,"genre":genre,"bookdesc":bookdesc,
											  "stocks":stocks,"price":price}),
					xmlhttp = new XMLHttpRequest();
			    xmlhttp.onreadystatechange = function() {
		        	//prevent multiple requests on
		            if (this.readyState == 4 && this.status == 200) {
		            	//hide the form on success except for the table
		            	hideOpenedOptions(1);
		            	var editedBook = document.createElement("H3");
		            	editedBook.innerHTML = this.responseText;
		            	editedBook.setAttribute('id','editedBookMsg');
		            	editedBook.classList.add("flash","animated");
		            	var modalContent = document.getElementById('productsOptionsView');
		                modalContent.appendChild(editedBook);
		                updateBookTable(bookIDN);
		            }
		        };
		        xmlhttp.open("POST", "editBook.php");
		        xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
		        xmlhttp.send(editBook);

			});
		        function updateBookTable(bookIDN){
		        	var xmlhttp = new XMLHttpRequest(),
	        	      	lookUpBookID = `bookIDN=${bookIDN}`;
        	        xmlhttp.onreadystatechange = function() {
        	            if (this.readyState == 4 && this.status == 200) {
	        	            var list = document.createElement('UL');
	        	            list.innerHTML = this.responseText;
	        	            list.style.display = "none";
	        	            for(var i = 0; list.children[i] != null; i++) {
        	              		document.getElementsByTagName('tr')[1].children[i].innerHTML = list.children[i].innerHTML;
        	              		document.getElementsByTagName('tr')[1].children[i].classList.add("fadeIn","animated");
	        	            }
	        	            //update the form with the new values
							editFormGetValues();
        	            }
        	        };
        	        xmlhttp.open("POST", "updateBookTable.php");
        	        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        	        xmlhttp.send(lookUpBookID);
		        }
		}
	}

	function previousInp(e) {
		if(e.keyCode == 38){
			//get the value of the previous input
			if(revctr == 0) { revctr = ctr; }
			var formGroup = document.getElementsByClassName('form-group')[revctr-1];
			var inpAreaValue = formGroup.firstElementChild.firstElementChild.value;
			document.getElementById('inputTxt').value = inpAreaValue;
			revctr--;
			//Temporarily remove focus to have cursor go to end of line instead of beginning
			document.getElementById('inputTxt').removeEventListener('blur',keepFocus,true);
			document.getElementById('inputTxt').blur();
			setTimeout(function() {
				document.getElementById('inputTxt').focus();
			}, 10);
			document.getElementById('inputTxt').addEventListener('blur',keepFocus,true);
		}
	}
}