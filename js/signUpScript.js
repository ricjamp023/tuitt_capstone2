//set a script to limit length of contact number accg to first character (+ - 13 or 0 - 11)
setNumLength(document.querySelector("input[name='contactNum']"));
//set a script to autocomplete province name
autoProvince(document.querySelector("input[name='province']"));

function setNumLength(contactNum) {
	contactNum.addEventListener('input',function(){
		var num = contactNum.value[0];
		if (num == "+")
			contactNum.setAttribute("maxlength","13");
		else if (num == "0")
			contactNum.setAttribute("maxlength","11");
	});
}

function autoProvince(provinceInp) {
	var phProvinces = ["Abra","Agusan del Norte","Agusan del Sur","Aklan","Albay","Antique","Apayao","Aurora","Basilan","Bataan","Batanes","Batangas","Benguet","Biliran","Bohol","Bukidnon","Bulacan","Cagayan","Camarines Norte","Camarines Sur","Camiguin","Capiz","Catanduanes","Cavite","Cebu","Compostela Valley","Cotabato","Davao del Norte","Davao del Sur","Davao Occidental","Davao Oriental","Dinagat Islands","Eastern Samar","Guimaras","Ifugao","Ilocos Norte","Ilocos Sur","Iloilo","Isabela","Kalinga","La Union","Laguna","Lanao del Norte","Lanao del Sur","Leyte","Maguindanao","Marinduque","Masbate","Misamis Occidental","Misamis Oriental","Mountain Province","Negros Occidental","Negros Oriental","Northern Samar","Nueva Ecija","Nueva Vizcaya","Occidental Mindoro","Oriental Mindoro","Palawan","Pampanga","Pangasinan","Quezon","Quirino","Rizal","Romblon","Samar","Sarangani","Siquijor","Sorsogon","South Cotabato","Southern Leyte","Sultan Kudarat","Sulu","Surigao del Norte","Surigao del Sur","Tarlac","Tawi-Tawi","Zambales","Zamboanga del Norte","Zamboanga del Sur","Zamboanga Sibugay","Metro Manila"];
	var provArrLength = phProvinces.length;
	var currentFocus;
	provinceInp.addEventListener('input',showProvince);
	provinceInp.addEventListener('keydown',chooseMatch);
	document.addEventListener('click',closeAllLists);

	function showProvince(input) {
		var value = this.value;
		var valueLength = value.length;
		//close already open lists
		closeAllLists();
		//if there is no value
		if (!value) {return false;}
		currentFocus = -1;
		//create a container for the autocomplete items
		var compItem = document.createElement("DIV");
		compItem.setAttribute("id",this.id + "autocomplete-list");
		compItem.setAttribute("class", "autocomplete-items");
		//append the div to the input box
		this.parentNode.appendChild(compItem);
		//loop through the provinces array comparing values of input and array
		for (var i = 0; i < provArrLength; i++) {
			//check array values as input text lengthens increases (substr), convert to uppercase to ignore case
			if (phProvinces[i].substr(0,valueLength).toUpperCase() == value.toUpperCase()){
				//create a div for matching elements
				var match = document.createElement("DIV");
				//make matching letters bold
				match.innerHTML = "<strong>" + phProvinces[i].substr(0,valueLength) + "</strong>";
				match.innerHTML += phProvinces[i].substr(valueLength);
				//create a container for current input text value
				match.innerHTML += "<input type='hidden' value='" + phProvinces[i] + "'>";
				//when the value is clicked, replace current input text
				match.addEventListener('click',function(e){
					provinceInp.value = this.getElementsByTagName("input")[0].value;
					//close the list once a value has been chosen
					closeAllLists();
				});
				//add the match to the current list of items
				compItem.appendChild(match);
			}
		}
	}

	function chooseMatch(e) {
		var list = document.getElementById(this.id + "autocomplete-list");
		//if there is a list, get all the list items to the variable
		if (list) { list = list.getElementsByTagName("DIV");}
		//if the down arrow key is pressed
		if (e.keyCode == 40) {
			//currentFocus is increased (start: -1 to 0)
			currentFocus++;
			//highlight current item
			addActive(list);
		}
		//if the up arrow key is pressed
		else if (e.keyCode == 38) {
			//currentFocus is decreased (start: -1 to -2)
			currentFocus--;
			//highlight current item
			addActive(list);
		}
		//if the enter key is pressed, act as if a click event was done
		else if (e.keyCode == 13) {
			//prevent form submission
			e.preventDefault();
			if (currentFocus > -1) {
				//if there is a list
				if (list) { list[currentFocus].click(); }
			}
		}
	}

	function addActive(list) {
		if (!list) { return false; }
		//remove current active class
		removeActive(list);
		//if at the end of the list of matches
		if (currentFocus >= list.length) { currentFocus = 0; }
		//if at the start if the list of matches
		if (currentFocus < 0) { currentFocus = (list.length-1); }
		list[currentFocus].classList.add("autocomplete-active");
	}

	function removeActive(list) {
		var i, l = list.length;
		for(i=0;i<l;i++){
			list[i].classList.remove("autocomplete-active");
		}
	}

	function closeAllLists(elem) {
		//ignores the passed elem
		var x = document.getElementsByClassName("autocomplete-items"),
			i, l = x.length;
		for(i=0;i<l;i++)
			if (elem != x[i] && elem != provinceInp) 
				x[i].parentNode.removeChild(x[i]);
	}
}