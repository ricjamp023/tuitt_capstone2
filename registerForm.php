<?php

	//initialize variables for db table as empty
	$userLogin = $email = $userPass = $userRepPass = $fname = $lname = $contactNum = $address = $city = $province = $zipcode = "";
	//initialize error text variables into an array for easy checking
	$errors['nameErr'] = $errors['emailErr'] = $errors['passErr'] = $errors['reppassErr'] = $errors['fnameErr'] = $errors['lnameErr'] = $errors['contactErr'] = $errors['addressErr'] = $errors['cityErr'] = $errors['provinceErr'] = $errors['zipcodeErr'] = $repeatUser = "";
	//variable for checking if ready to push to db
	$isEmpty = "No Errors";

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		//username validation
		if(empty($_POST['username']))
			$errors['nameErr'] = "Please provide a username";
			else {
				$userLogin = test_input($_POST['username']); 
				// check if name only contains alphanum
				    if (!preg_match("/^[a-zA-Z0-9]*$/",$userLogin)) {
				      $errors['nameErr'] = "Only letters and numbers are allowed";
				    }
				}
		//email validation
		if(empty($_POST['email'])) 
			$errors['emailErr'] = "We need your email as a means to contact you";
			else {
				$email = test_input($_POST['email']); 
				// check if e-mail address is well-formed
				    if (!filter_var($email, FILTER_VALIDATE_EMAIL))
				    	$errors['emailErr'] = "Oops, that's not a valid email address";
			}
		//pasword validation
		if(empty($_POST['password'])) 
			$errors['passErr'] = "For your account's security, please provide a password";
			else {
				$userPass = $_POST['password'];
				if(strlen($userPass) < 6)
					$errors['passErr'] = "Please provide a password that is at least 6 characters long";
			}
		//confirm password validation
		if(!empty($_POST['password']) && (empty($_POST['reppassword'])))
			$errors['reppassErr'] = "Please confirm your password";
			else
				$userRepPass = $_POST['reppassword'];
				if (!empty($userPass) && ($userPass != $userRepPass))
					$errors['reppassErr'] = "It seems your passwords don't match";
		//name validation
		if(empty($_POST['fname'])) 
			$errors['fnameErr'] = "Hi, we'd have to call you by something, Mr./Ms.";
			else
				$fname = test_input($_POST['fname']);
		if(empty($_POST['lname'])) 
			$errors['lnameErr'] = "Hi, we'd have to call you by something, Mr./Ms.";
			else
				$lname = test_input($_POST['lname']);
		//contact number validation
		if(empty($_POST['contactNum']))
			$errors['contactErr'] = "We need your phone number to contact you for your orders";
			else {
				$contactNum = $_POST['contactNum']; 
				// check if phone number is valid
				    if (!preg_match("/^[+0-9]*$/",$contactNum)) {
				      $errors['contactErr'] = "Oops, that's not a valid phone number";
				    }
				}
		//address validation
		if(empty($_POST['address']))
			$errors['addressErr'] = "We need your address to accurately send your orders";
			else
				$address = $_POST['address'];
		//city validation
		if(empty($_POST['city']))
			$errors['cityErr'] = "We need your city address to accurately send your orders";
			else {
				$city = test_input($_POST['city']); 
				// check if cityname only contains alphanum
				    if (!preg_match("/^[a-zA-Z0-9 ]*$/",$city)) {
				      $errors['cityErr'] = "Oops, that's not a valid city name";
				    }
				}
		//province validation
		if(empty($_POST['province']))
			$errors['provinceErr'] = "We need your provincial address to accurately send your orders";
			else {
				$province = test_input($_POST['province']); 
				// check if province name only contains alphanum
				    if (!preg_match("/^[a-zA-Z0-9 ]*$/",$province)) {
				      $errors['provinceErr'] = "Oops, that's not a valid province name";
				    }
				}
		//zipcode validation
		if(!empty($_POST['zipcode'])) {
			$zipcode = test_input($_POST['zipcode']); 
			// check if zipcode is only numbers
			    if (!preg_match("/^[0-9]*$/",$zipcode)) {
			      $errors['zipcodeErr'] = "Oops, that's not a valid zipcode";
			    }
		}
		//insert into db once no more errors
		foreach($errors as $errName => $errVal) {
			if($errVal != "")
				$isEmpty = "Errors abound";
		}

		if($isEmpty != "Errors abound"){
			//initialize server user to php variables
			$servername = "localhost";
			$username = "ricjamp";
			$password = "";
			$dbname = "bookthrift";

			try {
				//create connection
				$conn = new PDO("mysql:host=$servername;dbname=$dbname",$username,$password);
				//PDO error mode to exception
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				//post values
				$userLogin = $_POST['username'];
				$email = $_POST['email'];
				//check if already registered
				$sql = "select userLogin from bibliophile where userLogin = '$userLogin' or email = '$email'";
				$stmt = $conn->prepare($sql);
				$stmt->execute();
				$stmt->setFetchMode(PDO::FETCH_ASSOC);
				if ($row = $stmt->fetch()){
					$repeatUser = "Sorry, that username/email is already taken. <br> Would you like to Login instead? Click <a href='#'>here</a>";
				}
				else {
					$userPass = password_hash($_POST['password'],PASSWORD_DEFAULT);
					$fname = $_POST['fname'];
					$lname = $_POST['lname'];
					$contactNum = $_POST['contactNum'];
					$address = $_POST['address'];
					$city = $_POST['city'];
					$province = $_POST['province'];
					$zipCode = $_POST['zipcode'];
					//insert into users table
					$sql = "insert into bibliophile (userLogin,email,userPass,firstName,lastName,contactNum,addressLine,city,province,zip_code) values('$userLogin','$email','$userPass','$fname','$lname','$contactNum','$address','$city','$province',$zipCode)";
					//exec() because no results are returned
					$conn->exec($sql);
					header('Location: completeReg.php');
				}
			}
			catch(PDOException $e) {
				echo $sql . "<br>" . $e->getMessage();
				}

			$conn = null;
		}
	}

	//sanitize data
	function test_input($data) {
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}

?>
	
<!DOCTYPE html>
<html>
<head>
	<title>Bookthrift Sign Up</title>
</head>

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<style type="text/css">
	
	#signUpCont { margin-top: 40px;	}
	#signUpCont h6 { color: red; }
	#formColumn { margin: 40px auto; border: double black 2px}
	#formColumn form { padding: 10px; }
	#formColumn h3 { text-align: center; margin: 30px auto; }
	#formColumn h3.personDetails { margin-top: 0; }
	.invalid-input { font-size: 80%; color: red; width: 100%; margin-top: .25rem; }
	.submitFormBtn { display: block; margin: 20px auto;	}
	/*HR Style from css-tricks.com*/
	hr.style-seven { overflow: visible; /* For IE */ height: 30px; border-style: solid; border-color: rgba(0,0,0,0.5); border-width: 1px 0 0 0; border-radius: 20px; margin-bottom: 0;} 
	hr.style-seven:before { /* Not really supposed to work, but does */ display: block; content: ""; height: 30px; margin-top: -31px; border-style: solid; border-color: rgba(0,0,0,0.5); border-width: 0 0 1px 0; border-radius: 20px; }
	.autocomplete { position: relative; display: inline-block; }
	.autocomplete-items { position: absolute; top: 100%; left: 0; right: 0; }
	.autocomplete-active { background-color: DodgerBlue !important; color: #ffffff; }

</style>

<body>

	<div id='signUpCont' class='text-center'>

		<h1>Thank you for your interest in us!</h1>
			<h4>We're going to need a few things from you to complete your registration.</h4>
				<h6>*Please note that all fields are required.</h6>

		<div class='container-fluid'>
			<div class="row">
				<!-- Empty Column for left space -->
				<div class="col-xs-2"></div>
				<div id="formColumn" class="col-xs-8 text-left">
					<!-- Disable client-side validation, use PHP instead -->
					<form action='<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>' method='post' novalidate>
						<div class="invalid-input text-center">
							<?php echo $repeatUser; ?>
						</div>
						<h3>Enter your Login Details here</h3>
						<div class='form-row'>
					    	<div class='form-group col-md-6'>
					    		<label for='username'>Username <small>(6-15 characters, alphanumeric)</small></label>
					    			<input type='text' class='form-control' name='username' placeholder='e.g. booklover01' maxlength='15' size='15' value='<?php echo $userLogin; ?>'autofocus>
					    			<div class="invalid-input">
					    				<?php echo $errors['nameErr']; ?>
					    			</div>
					   		</div>
					  	    <div class='form-group col-md-6'>
					     		<label for='email'>Email Address</label>
					       			<input type='email' class='form-control' name='email' placeholder='e.g. booklover@email.com' value='<?php echo $email; ?>'>
					       			<div class="invalid-input">
					       				<?php echo $errors['emailErr']; ?>
					       			</div>
					    	</div>
					  	</div>
				  		<div class='form-row'>
				  	    	<div class='form-group col-md-6'>
				  	    		<label for='password'>Password <small>(Minimum of 6 characters)</small></label>
				  	    			<input type='password' class='form-control' name='password'>
				  	    			<div class="invalid-input">
				  	    				<?php echo $errors['passErr']; ?>
				  	    			</div>
				  	   		</div>
				  	  	    <div class='form-group col-md-6'>
				  	     		<label for='reppassword'>Repeat Password</label>
				  	       			<input type='password' class='form-control' name='reppassword'>
				  	       			<div class="invalid-input">
				  	       				<?php echo $errors['reppassErr']; ?>
				  	       			</div>
				  	    	</div>
				  	  	</div>
			  	  		<hr class='style-seven'>
				  	  	<h3 class='personDetails'>We'd like to know more about you...</h3>
						<div class='form-row'>
					    	<div class='form-group col-md-6'>
					    		<label for='fname'>First Name</label>
					    			<input type='text' class='form-control' name='fname' value='<?php echo $fname; ?>'>
					    			<div class="invalid-input">
					    				<?php echo $errors['fnameErr']; ?>
					    			</div>
					   		</div>
					  	    <div class='form-group col-md-6'>
					     		<label for='lname'>Last Name</label>
					       			<input type='text' class='form-control' name='lname' value='<?php echo $lname; ?>'>
					       			<div class="invalid-input">
					       				<?php echo $errors['lnameErr']; ?>
					       			</div>
					    	</div>
					  	</div>
						<div class='form-group'>
							<label for='contactNum'>Contact Number</label>
						    	<input type='text' class='form-control' name='contactNum' value='<?php echo $contactNum; ?>'>
						    	<div class="invalid-input">
						    		<?php echo $errors['contactErr']; ?>
						    	</div>
						</div>
					  	<div class='form-group'>
					  		<label for='address'>Address</label>
					  	    	<input type='text' class='form-control' name='address' value='<?php echo $address; ?>'>
					  	    	<div class="invalid-input">
						    		<?php echo $errors['addressErr']; ?>
						    	</div>
					  	</div>
					 	<div class='form-row'>
					   		<div class='form-group col-md-6'>
					        	<label for='city'>City</label>
					      			<input type='text' id='city' class='form-control' name='city' value='<?php echo $city; ?>'>
		      			  	    	<div class="invalid-input">
		      				    		<?php echo $errors['cityErr']; ?>
		      				    	</div>
					    	</div>
					    	<div class='form-group col-md-4'>
					      		<label for='province'>Province</label>
					      			<input type='text' id='province' class='form-control' name='province' value='<?php echo $province; ?>' autocomplete='off'>
		      			  	    	<div class="invalid-input">
		      				    		<?php echo $errors['provinceErr']; ?>
		      				    	</div>
					    	</div>
					    	<div class='form-group col-md-2'>
					    		<label for='zipcode'>Zip Code</label>
					      			<input type='text' id='zipCode' class='form-control' name='zipcode' maxlength='4' size='4' value='<?php echo $zipcode; ?>'>
		      			  	    	<div class="invalid-input">
		      				    		<?php echo $errors['zipcodeErr']; ?>
		      				    	</div>
					    	</div>
					    </div>
					 	<button type='submit' class='btn btn-primary submitFormBtn'>Register</button>
					</form>
				</div>
				<!-- Empty Column for right space -->
				<div class="col-xs-2"></div>
			</div> <!-- row -->
		</div> <!-- form container-fluid -->
	</div> <!-- page container -->

<script type="text/javascript" src="js/signUpScript.js"></script>

</body>
</html>
