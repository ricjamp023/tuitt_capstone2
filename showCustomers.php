<?php 
	//initialize server user to php variables
			$servername = "localhost";
			$username = "ricjamp";
			$password = "";
			$dbname = "bookthrift";
			try {
				//create connection
				$conn = new PDO("mysql:host=$servername;dbname=$dbname",$username,$password);
				//PDO error mode to exception
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				//don't include admin
				$sql = "select userLogin,email,firstName,lastName,contactNum,province,reg_date from bibliophile where userCred != 'admin'";
				$stmt = $conn->prepare($sql);
				$stmt->execute();
				$stmt->setFetchMode(PDO::FETCH_ASSOC);
				echo "<table id='customersTable' class='table text-center'>
						<thead>
							<tr>
								<th scope='col'>Username</th>
								<th scope='col'>Email Address</th>
								<th scope='col'>First Name</th>
								<th scope='col'>Last Name</th>
								<th scope='col'>Contact Number</th>
								<th scope='col'>Province</th>
								<th scope='col'>Registration Date</th>
							</tr>
						</thead>
						<tbody>";
				//while there is data. Else display error.
				// Set a ctr to check if there is data
				$ctr = 0;
				while ($row = $stmt->fetch()){
					echo "<tr>
							<td>{$row['userLogin']}</td>
							<td>{$row['email']}</td>
							<td>{$row['firstName']}</td>
							<td>{$row['lastName']}</td>
							<td>{$row['contactNum']}</td>
							<td>{$row['province']}</td>
							<td>{$row['reg_date']}</td>
						</tr>";
					$ctr++;
				}
				if($ctr == 0) {
					echo "<tr><td colspan='7'>No data found</td></tr>";
				}
				//close table
				echo "</tbody></table>";
			}
			catch(PDOException $e) {
				echo $sql . "<br>" . $e->getMessage();
				}
			// close PDO connection
			$conn = null;
?>