<?php 
	//initialize server user to php variables
	$servername = "localhost";
	$username = "ricjamp";
	$password = "";
	$dbname = "bookthrift";

	//initialize variables for db table as empty
	$userLogin = $email = $userPass = $fname = $lname = $contactNum = $address = $city = $province = $zipcode = "";

	try {
		//create connection
		$conn = new PDO("mysql:host=$servername;dbname=$dbname",$username,$password);
		//PDO error mode to exception
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		//post values
		$userLogin = $_POST['username'];
		$email = $_POST['email'];
		$userPass = password_hash($_POST['password'],PASSWORD_DEFAULT);
		$fname = $_POST['fname'];
		$lname = $_POST['lname'];
		$contactNum = $_POST['contactNum'];
		$address = $_POST['address'];
		$city = $_POST['city'];
		$province = $_POST['province'];
		$zipCode = $_POST['zipcode'];
		//insert into users table
		$sql = "insert into bibliophile (userLogin,email,userPass,firstName,lastName,contactNum,addressLine,city,province,zip_code) values('$userLogin','$email','$userPass','$fname','$lname','$contactNum','$address','$city','$province',$zipCode)";
		//exec() because no results are returned
		$conn->exec($sql);
		echo "<h1 class='text-center'>Thank you for registering with us!</h1>
				<h3>We will redirect you to our main page in a few seconds...</h3>";
		header("refresh:3;url=mainSite.php");
		}
	catch(PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
		}

	$conn = null;

?>