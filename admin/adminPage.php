<?php 
	//check authentication
	if(!isset($_GET['admin'])) { 
		header("refresh: 5; url=../index.php");
		echo "<h1 style='text-align: center; color: red; font-weight: bolder; font-size: 5rem;'>
				ACCESS DENIED
				<br>
				Bye, Bye
				<br> :)
			  </h1>
			  <h1 id='denied' style='text-align: center; color: red; font-weight: bolder; font-size: 5rem;'></h1>
			  <script language='javascript'>
				window.onload = function() {
					var i = 5;
					setInterval(function(){
						document.getElementById('denied').innerHTML = i;
						i--;
					},900);
				}
			  </script>";
	}else {
		echo "<script language='javascript'>
				window.onload = function() {
					document.getElementById('adminTerminal').style.display = 'block';
				}
			  </script>";
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Admin Page</title>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<!-- Animate CSS -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">

	<style type="text/css">
		body { background-color: #3F3F3F; }
		#adminTerminal { display:none; }
		.termTxt { color: #B2B2FF;margin: 0; }
		.adminInp { background: transparent; border: none; caret-color: white; color: white;}
		.adminInp:focus { outline: none; background: transparent; }
		#adminForm>.form-group { margin-bottom: 0; }
		#termBtn { display: none; }
		ul { list-style-type: none; margin-bottom: 0; color: #B2B2FF;}
		#customersTable { background-color: #3F3F3F; color: #B2B2FF; }
		table img { max-height: 150px !important; }
		table td { vertical-align: unset !important; }
		#modalProductsWindow { display: none; overflow-y: auto; background-color: rgba(255,255,255,0.1); position: fixed; top: 0; left: 0; right: 0; bottom: 0; padding-bottom: 2rem;}
		#modalProductsContent { background-color: skyblue; }
		.modalProductsTitle { display: inline-block; margin-top: 1rem; margin-left: 2rem;}
		/*Based on Bootstrap's Close class*/
		.closeModal { float: right; background: none; border: none; font-size: 2rem; font-weight: 700; line-height: 1; color: #000; text-shadow: 0 1px 0 #fff; opacity: .5; }
		#productsOptions .col { margin-bottom: 0.5rem; }
		#productsOptions .col .btn { color: black; }
		#addProductsFormCont, #editProductsFormCont { display: none; }
		#addProductsFormCol, #editProductsFormCol { margin: 0 auto; }
		#addBookBtn,#editBookBtn { display: block; margin: 0.4rem auto; }
		#addedBookMsg, #editedBookMsg, #removedBookMsg { text-align: center; padding: 30px; }
		#removeConfirmation { display: none; position: absolute; z-index: 99; top: 40%; left:0; right: 0; margin: auto; width: 20rem; border: 2px solid black;}
		#removeBookConfirmed { float: left; margin-bottom: 1rem; }
		#removeBookCancel { float: right; margin-bottom: 1rem; }
		option[disabled] { font-weight: bold; color: black; margin-left: auto; margin-right: auto; }
		#nextBook { float:right; }
		#prevBook { float:left; }
	</style>

</head>
<body>

	<div id="adminTerminal" class="container-fluid">
		<p id="introTxt" class="termTxt">Hello, Admin.<br>
		Type "help" for a list of commands.</p>		
		<form id="adminForm" method="post" autocomplete="off">
			<div class="form-group">
				<label class="termTxt">admin@bookthrift ~ $ <input id="inputTxt" class="adminInp" name="inputTxt" type="text" autofocus></label>
			</div>
			<button id="termBtn" type="submit"></button>
		</form>		
		<div id="modalProductsWindow" class="container-fluid fadeIn animated">
			<div id="modalProductsContent">
				<h2 class="modalProductsTitle">The BookThrift Books</h2>
				<button type="button" class="closeModal" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				</button>
				<hr>
				<div id="productsOptions" class="row text-center">
					<div class="col">
						<button type="button" id="viewProds" class="btn btn-info">View Products</button>
					</div>
					<div class="col">
						<button type="button" id="addProds" class="btn btn-primary">Add Products</button>
					</div>
				</div>	
				<hr>
				<div id="productsOptionsView" class="container-fluid">
					<div id="addProductsFormCont" class="row">
						<!-- Empty Left Column style -->
						<div class="col-xs-1"></div>
						<div id="addProductsFormCol" class="col-xs-10 text-left">
							<form id="addProductsForm">
								<div class="form-group">
									<label for="title">Title</label>
									<input type="text" class="form-control" name="title">
								</div>
								<div class="form-group">
									<label for="image">Image</label>
										<input type="file" class="form-control" name="image" accept="image/*">
								</div>
								<div class="form-group">
									<label for="author">Author</label>
										<input type="text" class="form-control" name="author">
								</div>
								<div class="form-group">
									<label for="publisher">Publisher</label>
										<input type="text" class="form-control" name="publisher">
								</div>
								<div class="form-group">
									<label for="genre">Genre</label>
										<select class="form-control" name="genre">
											<option disabled>Fiction</option>
											<?php 
											$fic = ["Classic","Comics/Graphic novel","Crime/Detective","Fable","Fairy tale","Fan fiction","Fantasy","Folklore","Historical fiction","Horror","Humor","Legend","Magical realism","Meta fiction","Mystery","Mythology","Mythopoeia","Picture book","Realistic fiction","Science fiction","Short story","Suspense/Thriller","Tall tale","Western"]; 
											for($i = 0; $i < sizeof($fic); $i++)
												echo "<option>$fic[$i]</option>";
											?>
											<option disabled>Non-Fiction</option>
											<?php 
											$nonfic = ["Biography","Essay","Instruction manual","Journalism","Lab Report","Memoir","Personal Narrative","Reference","Self-help","Speech","Textbook"];
											for($i = 0; $i < sizeof($nonfic); $i++)
												echo "<option>$nonfic[$i]</option>";
											?>
										</select>
								</div>
								<div class="form-group">
									<label for="bookdesc">Description</label>
										<textarea name="bookdesc" class="form-control"></textarea>
								</div>
								<div class="form-group">
									<label for="stocks">Quantity</label>
										<input type="number" class="form-control" name="stocks">
								</div>
								<div class="form-group">
									<label for="price">Price</label>
										<input type="number" class="form-control" name="price" step="0.01" min="0" placeholder="Php.">
								</div>
								<button id="addBookBtn" type="submit" class="btn btn-primary">Add Book</button>
							</form>
						</div>
						<!-- Empty Right Column style -->
						<div class="col-xs-1"></div>
					</div> <!-- Add Products Form Container Row -->
					<div id="editProductsFormCont" class="row">
						<!-- Empty Left Column style -->
						<div class="col-xs-1"></div>
						<div id="editProductsFormCol" class="col-xs-10 text-left">
							<form id="editProductsForm">
								<div class="form-group">
									<label for="title">Title</label>
									<input type="text" class="form-control" name="edittitle">
								</div>
								<div class="form-group">
									<label for="image">Image</label>
										<input type="file" class="form-control" name="editimage" accept="image/*">
								</div>
								<div class="form-group">
									<label for="author">Author</label>
										<input type="text" class="form-control" name="editauthor">
								</div>
								<div class="form-group">
									<label for="publisher">Publisher</label>
										<input type="text" class="form-control" name="editpublisher">
								</div>
								<div class="form-group">
									<label for="genre">Genre</label>
										<select class="form-control" name="editgenre">
											<option disabled>Fiction</option>
											<?php 
											$fic = ["Classic","Comics/Graphic novel","Crime/Detective","Fable","Fairy tale","Fan fiction","Fantasy","Folklore","Historical fiction","Horror","Humor","Legend","Magical realism","Meta fiction","Mystery","Mythology","Mythopoeia","Picture book","Realistic fiction","Science fiction","Short story","Suspense/Thriller","Tall tale","Western"]; 
											for($i = 0; $i < sizeof($fic); $i++)
												echo "<option>$fic[$i]</option>";
											?>
											<option disabled>Non-Fiction</option>
											<?php 
											$nonfic = ["Biography","Essay","Instruction manual","Journalism","Lab Report","Memoir","Personal Narrative","Reference","Self-help","Speech","Textbook"];
											for($i = 0; $i < sizeof($nonfic); $i++)
												echo "<option>$nonfic[$i]</option>";
											?>
										</select>
								</div>
								<div class="form-group">
									<label for="bookdesc">Description</label>
										<textarea name="editbookdesc" class="form-control"></textarea>
								</div>
								<div class="form-group">
									<label for="stocks">Quantity</label>
										<input type="number" class="form-control" name="editstocks">
								</div>
								<div class="form-group">
									<label for="editprice">Price</label>
										<input type="number" class="form-control" name="editprice" step="0.01" min="0" placeholder="Php.">
								</div>
								<button id="editBookBtn" type="submit" class="btn btn-primary">Edit Book</button>
							</form>
						</div>
						<!-- Empty Right Column style -->
						<div class="col-xs-1"></div>
					</div> <!-- Add Products Form Container Row -->
				</div> <!-- Products Options View Container Fluid -->
			</div> <!-- Modal Products Content -->
		</div> <!-- Modal Products Window -->
		<div id="removeConfirmation" class="card">
			<div class="card-body">
				<h5 class="card-title">Confirm Removal of Book</h5>
				<p>Are you sure you wish to remove this book?</p>
				<div class="container">
					<button id="removeBookConfirmed" class="btn btn-danger">Yes</button>
					<button id="removeBookCancel" class="btn btn-secondary">No</button>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript" src="js/adminPageScript.js"></script>

</body>
</html>