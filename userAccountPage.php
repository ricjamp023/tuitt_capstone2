<?php 
	//session for autofill update info values
	session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Bibliophile Account</title>
	<style type="text/css">
		#bookshelf { border-left: 2rem solid transparent; border-right: 2rem solid transparent; }
		#bookshelf .card-body { min-height: 500px; }
		@media (min-width: 567px) and (max-width: 800px) {
			#bookshelf .card-body { min-height: 200px; }
		}
		#changeEmailBtn, #changePassBtn { display: block; }
		#changeEmailForm, #changePassForm { display: none; margin: 0.5rem; padding: 0.5rem; border: 2px double black;}
		.invalid-input { font-size: 80%; color: red; width: 100%; }
		.nav-link:hover { cursor: pointer; }
		#updateInfoForm { margin-top: 2rem; }
	</style>
</head>
<body>
	<?php include_once 'mainSiteNavbar.php'; ?>
	<div id="bookshelf" class="container-fluid">
		<div id="acctPage" class="card text-center">
			<div class="card-header">
		    	<ul class="nav nav-tabs card-header-tabs">
		    		<li class="nav-item">
		        		<button id="acctDetailsBtn" class="nav-link active" type="button">Your Account Details</button>
		      		</li>
		      		<li class="nav-item">
		        		<button id="orderBtn" class="nav-link" type="button">Your Order Details</button>
		      		</li>
		      		<li class="nav-item">
		        		<button class="nav-link" type="button">Your Cart</button>
		      		</li>
		    	</ul>
		  	</div>
			<div class="card-body">
			  	<div id="emailAndPassRow" class="row">
			  		<div class="col">
					    <button id="changeEmailBtn" class="btn btn-primary mx-auto" type="button">Change Email</button>
				    	<div id="changeEmailForm">	
							<div class="form-group">
								<label>Current Email
									<input type="email" class="form-control" name="currEmail" 
									value="<?php echo htmlspecialchars($_SESSION['userEmail']); ?>" readonly>
								</label>
							</div>
				    		<div class="form-group">
				    			<label>New Email
				    				<input type="email" class="form-control" name="newEmail"
				    				value="<?php echo ""; ?>">
				    			</label>
    			    			<div id="invalidNewEmail" class="invalid-input"></div>
				    		</div>
				    		<button id="saveNewEmail" class="btn btn-info" type="button">Save</button>
				    		<button id="cancelNewEmail" class="btn btn-info" type="button">Cancel</button>
				    	</div> <!-- #changeEmailForm -->
				    </div> <!-- Column for #changeEmailForm -->
				    <div class="col">
					    <button id="changePassBtn" class="btn btn-primary mx-auto" type="button">Change Password</button>
		    	    	<div id="changePassForm">
    			    		<div class="form-group">
    			    			<label>Please enter your current Password
    			    				<input type="password" class="form-control" name="currPass">
    			    			</label>
    			    		</div>
    			    		<div class="form-group">
    			    			<label>New Password
    			    				<input type="password" class="form-control" name="newPass">
    			    			</label>
    			    		</div>
    			    		<div class="form-group">
    			    			<label>Confirm New Password
    			    				<input type="password" class="form-control" name="newPassConf">
    			    			</label>
    			    			<div id="invalidNewPass" class="invalid-input"></div>
    			    		</div>
		    	    		<button id="saveNewPass" class="btn btn-info" type="button">Save</button>
		    	    		<button id="cancelNewPass" class="btn btn-info" type="button">Cancel</button>
		    	    	</div> <!-- #changePassForm -->
		    	    </div><!-- Column for #changePassForm -->
		    	</div> <!-- emailAndPass details row -->
		    	<hr>
		    	<form id="updateInfoForm" action='<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>' method='post' novalidate>
					<div class='form-row'>
				    	<div class='form-group col-md-6'>
				    		<label for='fname'>First Name</label>
				    			<input type='text' class='form-control' name='fname' value='<?php echo htmlspecialchars($_SESSION['firstName']); ?>'>
				    			<div class="invalid-input"></div>
				   		</div>
				  	    <div class='form-group col-md-6'>
				     		<label for='lname'>Last Name</label>
				       			<input type='text' class='form-control' name='lname' value='<?php echo htmlspecialchars($_SESSION['lastName']); ?>'>
				       			<div class="invalid-input"></div>
				    	</div>
				  	</div>
					<div class='form-group'>
						<label for='contactNum'>Contact Number</label>
					    	<input type='text' class='form-control' name='contactNum' value='<?php echo htmlspecialchars($_SESSION['contactNum']); ?>'>
					    	<div class="invalid-input"></div>
					</div>
				  	<div class='form-group'>
				  		<label for='address'>Address</label>
				  	    	<input type='text' class='form-control' name='address' value='<?php echo htmlspecialchars($_SESSION['addressLine']); ?>'>
				  	    	<div class="invalid-input"></div>
				  	</div>
				 	<div class='form-row'>
				   		<div class='form-group col-md-6'>
				        	<label for='city'>City</label>
				      			<input type='text' id='city' class='form-control' name='city' value='<?php echo htmlspecialchars($_SESSION['city']); ?>'>
	      			  	    	<div class="invalid-input"></div>
				    	</div>
				    	<div class='form-group col-md-4'>
				      		<label for='province'>Province</label>
				      			<input type='text' id='province' class='form-control' name='province' value='<?php echo htmlspecialchars($_SESSION['province']); ?>' autocomplete='off'>
	      			  	    	<div class="invalid-input"></div>
				    	</div>
				    	<div class='form-group col-md-2'>
				    		<label for='zipcode'>Zip Code</label>
				      			<input type='text' id='zipCode' class='form-control' name='zipcode' maxlength='4' size='4' value='<?php echo htmlspecialchars($_SESSION['zip_code']); ?>'>
	      			  	    	<div class="invalid-input"></div>
				    	</div>
				    </div>
				    <button type='submit' class='btn btn-primary'>Save Changes</button>
				</form>
			  </div> <!-- .card-body -->
		</div>
	</div>

<script type="text/javascript" src="js/signUpScript.js"></script>

<script type="text/javascript">

	userAccountControl(document.getElementById('changeEmailBtn'),document.getElementById('changePassBtn'),document.getElementById('updateInfoForm'));

	function userAccountControl(inpEmail,inpPass,infoForm) {
		//nested function to show change forms on btn click
		inpEmail.addEventListener('click',function(){
			var emailForm = document.getElementById('changeEmailForm');
			toggleDisplay(emailForm);
			//if email was updated prior, remove email success message
			if(document.getElementById('successEmailUpdate')){
				this.parentNode.removeChild(document.getElementById('successEmailUpdate'));
			}
			var newEmail = document.querySelector("input[name='newEmail']");
				newEmail.value = "";
				newEmail.focus();
			changeEmailBtn = this;
			toggleDisableBtn(changeEmailBtn);
				//save new email btn event listener
				document.getElementById('saveNewEmail').addEventListener('click',function(e){
					//prevent multiple event handlers from happening (button clicked > 1)
					e.stopImmediatePropagation();
					//make an ajax call to change email and show success msg
		        	var xmlhttp = new XMLHttpRequest(),
		        		//variable for php file to check what to update
		        		updateThis = "update=email",
		        		//variable to be updated
		        		email = "&email="+newEmail.value,
		        		//simple string to post to php
		        		toPost = updateThis+email;
			        xmlhttp.onreadystatechange = function() {
			            if (this.readyState === 4 && this.status === 200) {
			            	//check value of response
			            	var result = this.responseText;
			            	if(result === "Empty") {
			            		document.getElementById('invalidNewEmail').innerHTML = "Oops, it's empty.";
			            		newEmail.focus();
			            	}
			            	else if(result === "Invalid") {
			            		document.getElementById('invalidNewEmail').innerHTML = "Oops, that's an invalid email.";
			            		newEmail.focus();
			            	}
			            	else if(result === "Success"){
			            		var successMsg = document.createElement("H2"),
			            			successMsgTxt = document.createTextNode("Email Updated");
			            		successMsg.appendChild(successMsgTxt);
			            		successMsg.setAttribute("id","successEmailUpdate")
			            		toggleDisplay(emailForm);
			            		toggleDisableBtn(changeEmailBtn);
			            		changeEmailBtn.parentNode.insertBefore(successMsg, changeEmailBtn.nextElementSibling);
		        			}

			            }
			        };
			        xmlhttp.open("POST", "updateLogin.php");
			        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			        xmlhttp.send(toPost);
				});
				//cancel change btn event listener
				document.getElementById('cancelNewEmail').addEventListener('click',function(e){
					//prevent multiple event handlers from happening (button clicked > 1)
					e.stopImmediatePropagation();
					toggleDisplay(emailForm);
					toggleDisableBtn(changeEmailBtn);
				});
		});
		//nested function to show change forms on btn click
		inpPass.addEventListener('click',function(){
			var passForm = document.getElementById('changePassForm');
			toggleDisplay(passForm);
			//if email was updated prior, remove email success message
			if(document.getElementById('successPassUpdate')){
				this.parentNode.removeChild(document.getElementById('successPassUpdate'));
			}
			var currPass = document.querySelector("input[name='currPass']"),
				newPass = document.querySelector("input[name='newPass']"),
				newPassConf = document.querySelector("input[name='newPassConf']");
			//set focus to current password input box
			currPass.focus();
			newPass.value = "";
			newPassConf.value = "";
			changePassBtn = this;
			toggleDisableBtn(changePassBtn);
				//save new pass btn event listener
				document.getElementById('saveNewPass').addEventListener('click',function(e){
					//prevent multiple event handlers from happening (button clicked > 1)
					e.stopImmediatePropagation();
					//check if confirm new password is same
					if(newPass.value !== newPassConf.value) {
				    	document.getElementById('invalidNewPass').innerHTML = "Your New Passwords are not the same.";
				    //check length of password if too short
				    } else if(newPass.value.length < 6) {
				    	document.getElementById('invalidNewPass').innerHTML = "Your New Password must be at least 6 characters long.";
				    	} else {
							//make an ajax call to change email and show success msg
				        	var xmlhttp = new XMLHttpRequest(),
				        		//variable for php file to check what to update
				        		updateThis = "update=userPass",
				        		//current password for checking
				        		pass = "&userPass="+document.querySelector("input[name='currPass']").value,
				        		//new password for updating
				        		newpass = "&newUserPass="+newPass.value;
				        		//simple string to post to php
				        		toPost = updateThis+pass+newpass;
					        xmlhttp.onreadystatechange = function() {
					            if (this.readyState === 4 && this.status === 200) {
					            	//check value of response
					            	var result = this.responseText;
				            		if(result === "Empty") {
				            			document.getElementById('invalidNewPass').innerHTML = "Oops, you missed something.";
				            			currPass.focus();
				            		}
					            	else if(result === "Invalid") {
					            		document.getElementById('invalidNewPass').innerHTML = "Oops, that isn't your current Password.";
					            		currPass.focus();
					            	}
					            	else if(result === "Success"){
					            		var successMsg = document.createElement("H2"),
					            			successMsgTxt = document.createTextNode("Password Updated");
					            		successMsg.appendChild(successMsgTxt);
					            		successMsg.setAttribute("id","successPassUpdate")
					            		toggleDisplay(passForm);
					            		toggleDisableBtn(changePassBtn);
					            		changePassBtn.parentNode.insertBefore(successMsg, changePassBtn.nextElementSibling);
				        			}
					            }
					        };
					        xmlhttp.open("POST", "updateLogin.php");
					        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
					        xmlhttp.send(toPost);
					    }
					});
				//cancel change btn event listener
				document.getElementById('cancelNewPass').addEventListener('click',function(e){
					//prevent multiple event handlers from happening (button clicked > 1)
					e.stopImmediatePropagation();
					toggleDisplay(passForm);
					toggleDisableBtn(changePassBtn);
				});
		});
		//changes saved for personal info
		infoForm.addEventListener('submit',function(e){
			e.preventDefault();
			e.stopImmediatePropagation();
			//get values from the form
			var updatedInfo = [],
			    check = 0;
			updatedInfo[0] = document.querySelector("input[name='fname']");
		    updatedInfo[1] = document.querySelector("input[name='lname']");
		    updatedInfo[2] = document.querySelector("input[name='contactNum']");
		    updatedInfo[3] = document.querySelector("input[name='address']");
		    updatedInfo[4] = document.querySelector("input[name='city']");
		    updatedInfo[5] = document.querySelector("input[name='province']");
		    updatedInfo[6] = document.querySelector("input[name='zipcode']");
			//check for empty values
			for(var i = 0, l = updatedInfo.length; i<l; i++) {
				if(updatedInfo[i].value === "") {
					updatedInfo[i].nextElementSibling.innerHTML = "Oops, it's empty";
					check = 1;
				}
			}
			//check for errors in contactNum and zipcode
			if(/[^+0-9]/.test(updatedInfo[2].value)) {
				updatedInfo[2].nextElementSibling.innerHTML = "That's not a correct phone number";
				check = 1;
			}
			if(/[^0-9]/.test(updatedInfo[6].value)) {
				updatedInfo[6].nextElementSibling.innerHTML = "That's not a correct zip code";
				check = 1;
			}
			if(check === 0) {
	    		//ajax request to add book and echo success message to a div
	    		//get the values
	    		var fname = updatedInfo[0].value,
	    			lname = updatedInfo[1].name,
	    			contactNum = updatedInfo[2].value,
	    			address = updatedInfo[3].value,
	    			city = updatedInfo[4].value,
	    			province = updatedInfo[5].value,
	    			zipcode = updatedInfo[6].value,
	    			updateUserStr = JSON.stringify({"fname":fname,"lname":lname,"contactNum":contactNum,"address":address,"city":city,"province":province,
	    									  "zipcode":zipcode}),
	    			xmlhttp = new XMLHttpRequest();
	    	        xmlhttp.onreadystatechange = function() {
	    	            if (this.readyState == 4 && this.status == 200) {
	    	            	var x = document.createTextNode(this.responseText);
	    	            	var y = document.createElement("SPAN");
	    	            	infoForm.appendChild(x);
	    	            }
	    	        };
	    	        xmlhttp.open("POST", "updateUserInfo.php");
	    	        xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
	    	        xmlhttp.send(updateUserStr);
			}
		});

		//event listener for shipping/personal details 
		// shipBtn.addEventListener('click',function(){
		// 	//move active class to shipping btn on card nav
		// 	//access nav-link dom object collection
		// 	var cardBtn = document.getElementsByClassName('nav-link');
		// 	for(var i = 0, l = cardBtn.length; i<l; i++) {
		// 		if(cardBtn[i].classList.contains("active"))
		// 			cardBtn[i].classList.remove("active");
		// 	shipBtn.classList.add("active");	
		// 	}
		// });

		function toggleDisplay(elem) {
			//display block if none
			if(elem.style.display === "none" || elem.style.display === "") 
				{ elem.style.display = "block"; }
				else { elem.style.display = "none"; }
		}

		function toggleDisableBtn(btn) {
			//disable or re-enable buttons
			if(btn.hasAttribute("disabled")) { btn.removeAttribute("disabled"); }
				else { btn.setAttribute("disabled",""); }
		}
	}

</script>
</body>
</html>