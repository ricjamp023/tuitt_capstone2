<?php 

//initialize server user to php variables
	$servername = "localhost";
	$username = "ricjamp";
	$password = "";
	$dbname = "bookthrift";

	try {
		//create connection
		$conn = new PDO("mysql:host=$servername;dbname=$dbname",$username,$password);
		//PDO error mode to exception
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		//post values
		//header for json
		header("Content-Type: application/json");
		// build a PHP variable from JSON sent using POST method
		$bookdata = json_decode(stripslashes(file_get_contents("php://input")),true);
		$title = $bookdata['title'];
		$image = $bookdata['image'];
		$author = $bookdata['author'];
		$publisher = $bookdata['publisher'];
		$genre = $bookdata['genre'];
		$bookdesc = $bookdata['bookdesc'];
		$stocks = $bookdata['stocks'];
		$price = $bookdata['price'];
		//insert into users table
		$sql = "insert into books (title,bookImg,author,publisher,subGenre,bookDesc,stocks,price) values('$title','$image','$author','$publisher','$genre','$bookdesc','$stocks','$price')";
		//exec() because no results are returned
		$conn->exec($sql);
		//get the mainGenre name from sub_genre table
		$sql = "update books b join sub_genre s on b.subGenre = s.subGenreName set b.mainGenre = s.mainGenre";
		//exec() because no results are returned
		$conn->exec($sql);
		echo "Book Added Successfully";
	}
	catch(PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
		}

	$conn = null;

?>