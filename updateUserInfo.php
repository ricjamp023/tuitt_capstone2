<?php

	//session to get username
	session_start();
	//initialize server user to php variables
	$servername = "localhost";
	$username = "ricjamp";
	$password = "";
	$dbname = "bookthrift";
	try {
		//variables for updating and data validation
		$userLogged = $_SESSION['userlogged'];
		//header for json
		header("Content-Type: application/json");
		//build a PHP variable from JSON sent using POST method
		$userdata = json_decode(stripslashes(file_get_contents("php://input")),true);
		//update Session values and variable data
		$_SESSION['firstName'] = $fname = $userdata['fname'];
		$_SESSION['lastName'] = $lname = $userdata['lname'];
		$_SESSION['contactNum'] = $contactNum = $userdata['contactNum'];
		$_SESSION['addressLine'] = $address = $userdata['address'];
		$_SESSION['city'] = $city = $userdata['city'];
		$_SESSION['province'] = $province = $userdata['province'];
		$_SESSION['zip_code'] = $zipcode = $userdata['zipcode'];
		//update user's table
		//create connection
		$conn = new PDO("mysql:host=$servername;dbname=$dbname",$username,$password);
		//PDO error mode to exception
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "update bibliophile set firstName='$fname',lastName='$lname',contactNum='$contactNum',addressLine='$address',city='$city',province='$province',zip_code='$zipcode' where userLogin='$userLogged'";
		//exec() because no results are returned
		$conn->exec($sql);
		echo "Profile Updated";
	}
	catch(PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}
	// close PDO connection
	$conn = null;
?>