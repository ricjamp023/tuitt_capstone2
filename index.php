<?php 
	session_start();
	//set a cookie to check multiple erroneous password logins
	setcookie("loginCtr","0");
?>
<!DOCTYPE html>
<html>
<head>
	<title>The Book Thrift</title>

	<style type="text/css">
		/*Background image from andreasrocha - https://www.patreon.com/andreasrocha*/
		#landingPage { background: url(https://pre00.deviantart.net/bfa9/th/pre/f/2016/111/b/7/old_library_a___patreon_illustration_pack_06_by_andreasrocha-d9zpkfb.jpg) no-repeat center center; background-size: cover; color: white; margin-bottom: 0; }
		#landingPage p { margin-right: 40%; }
		.input-group { border: 3px groove #421D0B; border-radius: .25rem; }
		.input-group * { color: black !important; }
	</style>
</head>

<body>
	<?php include_once 'mainSiteNavbar.php'; ?>
	<div id="bookshelf" class="container-fluid">
		<div id="landingPage" class="jumbotron jumbotron-fluid bg-dark">
			<div class="container text-left">
				<h2 class="bg-faded">We BUY and SELL Pre-Loved Books</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			</div>
		</div>
	</div>	
</body>
</html>