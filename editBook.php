<?php 

//initialize server user to php variables
	$servername = "localhost";
	$username = "ricjamp";
	$password = "";
	$dbname = "bookthrift";

	try {
		//create connection
		$conn = new PDO("mysql:host=$servername;dbname=$dbname",$username,$password);
		//PDO error mode to exception
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		//post values
		//header for json
		header("Content-Type: application/json");
		//build a PHP variable from JSON sent using POST method
		$bookdata = json_decode(stripslashes(file_get_contents("php://input")),true);
		$bookIDN = $bookdata['bookIDN'];
		$title = $bookdata['title'];
			//check if image was changed or not
			if($bookdata['image'] == " ") {
				$sql = "select bookImg from books where bookIDN='$bookIDN'";
				$stmt = $conn->prepare($sql);
				$stmt->execute();
				$stmt->setFetchMode(PDO::FETCH_ASSOC);
				while ($row = $stmt->fetch())
					$image = $row['bookImg'];
			} else {
				$image = $bookdata['image'];	
				}
		$author = ($bookdata['author'] == "") ? "Anonymous":$bookdata['author'];
		$publisher = $bookdata['publisher'];
		$genre = $bookdata['genre'];
		$bookdesc = $bookdata['bookdesc'];
		$stocks = $bookdata['stocks'];
		$price = $bookdata['price'];
		//update users table
		$sql = "update books set title='$title',bookImg='$image',author='$author',publisher='$publisher',subGenre='$genre',bookDesc='$bookdesc',stocks='$stocks',price='$price' where bookIDN='$bookIDN'";
		//exec() because no results are returned
		$conn->exec($sql);
		//get the mainGenre name from sub_genre table
		$sql = "update books b join sub_genre s on b.subGenre = s.subGenreName set b.mainGenre = s.mainGenre where bookIDN='$bookIDN'";
		//exec() because no results are returned
		$conn->exec($sql);
		echo "Book Edited Successfully";
	}
	catch(PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
		}

	$conn = null;

?>