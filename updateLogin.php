<?php

	//session to get username
	session_start();
	//initialize server user to php variables
	$servername = "localhost";
	$username = "ricjamp";
	$password = "";
	$dbname = "bookthrift";
	try {
		//variables for updating and data validation
		$userLogged = $_SESSION['userlogged'];
		$updateCol = $_POST['update'];
		$updateTo = $_POST["$updateCol"];
		$check = 0;
		//if user sent an empty value
		if($updateTo === "") { echo "Empty"; }
			else { //check what they're updating
				if($updateCol === "email") {
					//Validate email
					if(!filter_var($updateTo, FILTER_VALIDATE_EMAIL))
			    		echo "Invalid";
			    	else {
			    		$check = 1;
			    		$_SESSION['userEmail'] = $updateTo;
			    	}
			    }
			    if($updateCol === "userPass") {
			    	//create connection
			    	$conn = new PDO("mysql:host=$servername;dbname=$dbname",$username,$password);
			    	//PDO error mode to exception
			    	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			    	$sql = "select userPass from bibliophile where userLogin = '$userLogged'";
			    	$stmt = $conn->prepare($sql);
			    	$stmt->execute();
			    	$stmt->setFetchMode(PDO::FETCH_ASSOC);
			    	//Verify password
			    	if ($row = $stmt->fetch()){
			    		// check if password is correct. Else display error
			    		if (password_verify($updateTo,$row['userPass'])){
			    			//once verified, change variable to new password and $check to 1
			    			$updateTo = password_hash($_POST['newUserPass'],PASSWORD_DEFAULT);
			    			$check = 1;
			    		}else
			    			echo "Invalid";
			    	} 
			    }
			}
		if($check === 1) {
			//create connection
			$conn = new PDO("mysql:host=$servername;dbname=$dbname",$username,$password);
			//PDO error mode to exception
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "update bibliophile set $updateCol='$updateTo' where userLogin='$userLogged'";
			//exec() because no results are returned
			$conn->exec($sql);
			echo "Success";
		}
	}
	catch(PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
	}
	// close PDO connection
	$conn = null;
?>