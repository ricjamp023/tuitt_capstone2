<?php 
//initialize server user to php variables
	$servername = "localhost";
	$username = "ricjamp";
	$password = "";
	$dbname = "bookthrift";

	try {
		//create connection
		$conn = new PDO("mysql:host=$servername;dbname=$dbname",$username,$password);
		//PDO error mode to exception
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $subs = ["Biography",
             "Essay",
             "Instruction manual",
             "Journalism",
             "Lab Report",
             "Memoir",
             "Personal Narrative",
             "Reference",
             "Self-help",
             "Speech",
             "Textbook"];
    $subsDesc = ["narrative of a person\'s life; when the author is also the subject, this is an autobiography",
                 "short literary composition that reflects the author\'s outlook or point",
                 "an instructional book or booklet that is supplied with consumer products such as vehicles, home appliances, firearms, toys and computer peripherals",
                 "reporting on news and current events",
                 "a report of an experiment",
                 "factual story that focuses on a significant relationship between the writer and a person, place, or object; reads like a short novel",
                 "factual information about a significant event presented in a format that tells a story",
                 "such as a dictionary, thesaurus, encyclopedia, almanac, or atlas",
                 "information with the intention of instructing readers on solving personal problems",
                 "public address or discourse",
                 "authoritative and detailed factual description of a topic"];
		//insert values for fiction
		for($i = 0; $i<11; $i++) {
			$sql = "insert into sub_genre (subGenreName,mainGenre,subGenreDesc) values('$subs[$i]','Non-Fiction','$subsDesc[$i]')";
			//exec() because no results are returned
			$conn->exec($sql);
		}
			echo "YEY, Non-Fiction";
	}
	catch(PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
		}

	$conn = null;
?>