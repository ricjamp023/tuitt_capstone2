<?php 
//initialize server user to php variables
	$servername = "localhost";
	$username = "ricjamp";
	$password = "";
	$dbname = "bookthrift";

	try {
		//create connection
		$conn = new PDO("mysql:host=$servername;dbname=$dbname",$username,$password);
		//PDO error mode to exception
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $subs = ["Classic",
    		 "Comics/Graphic novel",
    		 "Crime/Detective",
    		 "Fable",
    		 "Fairy tale",
    		 "Fan fiction",
    		 "Fantasy",
    		 "Folklore",
    		 "Historical fiction",
    		 "Horror",
    		 "Humor",
    		 "Legend",
    		 "Magical realism",
    		 "Meta fiction",
    		 "Mystery",
    		 "Mythology",
    		 "Mythopoeia",
    		 "Picture book",
    		 "Realistic fiction",
    		 "Science fiction",
    		 "Short story",
    		 "Suspense/Thriller",
    		 "Tall tale",
    		 "Western"]; 
    $subsDesc = ["fiction that has become part of an accepted literary canon, widely taught in schools",
    			 "comic magazine or book based on a sequence of pictures (often hand drawn) and few words", 
    			 "fiction about a crime, how the criminal gets caught, and the repercussions of the crime", 
    			 "legendary, supernatural tale demonstrating a useful truth", 
    			 "story about fairies or other magical creatures",
    			 "fiction written by a fan of, and featuring characters from, a particular TV series, movie, or book", 
    			 "fiction with subplot(s), theme(s), major and minor characters, in which the narrative is presented in verse form (usually free verse)", 
    			 "the songs, stories, myths, and proverbs of a people or \"folk\" as handed down by word of mouth",
    			 "story with fictional characters and events in an historical setting",
    			 "fiction in which events evoke a feeling of dread and sometimes fear in both the characters and the reader",
    			 "usually a fiction full of fun, fancy, and excitement, meant to entertain and sometimes cause intended laughter; but can be contained in all genres",
    			 "story, sometimes of a national or folk hero, that has a basis in fact but also includes imaginative material",
    			 "story where magical or unreal elements play a natural part in an otherwise realistic environment",
    			 "uses self-reference to draw attention to itself as a work of art while exposing the \"truth\" of a story", 
    			 "fiction dealing with the solution of a crime or the revealing of secrets",
    			 "legend or traditional narrative, often based in part on historical events, that reveals human behavior and natural phenomena by its symbolism; often pertaining to the actions of the gods",
    			 "fiction in which characters from religious mythology, traditional myths, folklore and/or history are recast into a re-imagined realm created by the author",
    			 "picture storybook is a book with very little words and a lot of pictures; picture stories are usually for children",
    			 "story that is true to life",
    			 "story based on the impact of actual, imagined, or potential science, often set in the future or on other planets",
    			 "fiction of great brevity, usually supports no subplots",
    			 "fiction about harm about to befall a person or group and the attempts made to evade the harm",
    			 "humorous story with blatant exaggerations, such as swaggering heroes who do the impossible with nonchalance",
    			 "fiction set in the American Old West frontier and typically in the late eighteenth to late nineteenth century"];
		//insert values for fiction
		for($i = 0; $i<24; $i++) {
			$sql = "insert into sub_genre (subGenreName,mainGenre,subGenreDesc) values('$subs[$i]','Fiction','$subsDesc[$i]')";
			//exec() because no results are returned
			$conn->exec($sql);
		}
			echo "YEY, Fiction";
	}
	catch(PDOException $e) {
		echo $sql . "<br>" . $e->getMessage();
		}

	$conn = null;
?>