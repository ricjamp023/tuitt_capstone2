-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 28, 2018 at 11:13 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bookthrift`
--

-- --------------------------------------------------------

--
-- Table structure for table `bibliophile`
--

CREATE TABLE `bibliophile` (
  `userIDN` int(11) NOT NULL,
  `userLogin` varchar(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `userPass` varchar(255) NOT NULL,
  `userCred` varchar(20) NOT NULL DEFAULT 'user',
  `firstName` varchar(30) NOT NULL,
  `lastName` varchar(30) NOT NULL,
  `contactNum` varchar(30) NOT NULL,
  `addressLine` varchar(50) NOT NULL,
  `city` varchar(30) NOT NULL,
  `province` varchar(30) NOT NULL,
  `zip_code` int(4) UNSIGNED NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bibliophile`
--

INSERT INTO `bibliophile` (`userIDN`, `userLogin`, `email`, `userPass`, `userCred`, `firstName`, `lastName`, `contactNum`, `addressLine`, `city`, `province`, `zip_code`, `reg_date`) VALUES
(1, 'ricjamp', 'ricjamp@gmail.com', '$2y$10$fNWgOYrA6gsnldSJYDxqhuOONLoAteF3V7391NIgMhDrnvo2hcm0O', 'admin', 'Richard James', 'Pepito', '09270844245', '521 Jugan', 'Consolacion', 'Cebu', 6001, '2018-03-14 07:14:06'),
(2, 'booklover1', 'bookloverNumbah1@email.com', '$2y$10$WG2DZrwMo76LN6lc/tp91.tgQAaPKcU9bI6mDsoRGBc0DPmZ5ic.a', 'user', 'Book', 'Lover', '09111111111', 'Section 5', 'Aisle 3', 'Librarium', 1235, '2018-03-26 21:33:59'),
(3, 'booklover2', 'bukolover@email.com', '$2y$10$bApC9.lqM8iIzAUv2kI7B.peeUx/se.oeJ9woUV59ZHYTpeylDnY.', 'user', 'Buko', 'lname', '+632222222222', '222 Palmy Hilly Hills', 'Paradise', 'Sunnyville', 2222, '2018-03-27 06:47:08');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `bookIDN` int(10) NOT NULL,
  `title` varchar(50) NOT NULL,
  `bookImg` varchar(50) DEFAULT NULL,
  `author` varchar(30) NOT NULL DEFAULT 'Anonymous',
  `publisher` varchar(20) NOT NULL,
  `mainGenre` varchar(20) DEFAULT NULL,
  `subGenre` varchar(30) NOT NULL,
  `bookDesc` text,
  `stocks` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `price` decimal(7,2) UNSIGNED NOT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`bookIDN`, `title`, `bookImg`, `author`, `publisher`, `mainGenre`, `subGenre`, `bookDesc`, `stocks`, `price`, `add_date`) VALUES
(1, 'The Blank Book', 'book1.jpg', 'Anonymous', 'The World', 'Fiction', 'Fantasy', 'VOID', 1, '5.00', '2018-03-22 02:52:49'),
(2, 'The Black Book', 'blackbook.jpg', 'Indiscernible', 'Indiscernible', 'Fiction', 'Legend', 'Indiscernible', 1, '10.00', '2018-03-20 22:29:10'),
(3, 'BlankCoveredBook', 'book2.jpg', 'Eh', 'Self', 'Fiction', 'Short story', 'Story.', 3, '333.00', '2018-03-20 23:13:18'),
(4, 'Book4', 'book4.jpg', 'Author4', 'Publisher4', 'Fiction', 'Fantasy', 'Wow.', 4, '16.00', '2018-03-21 00:02:04'),
(5, 'Book5', 'book5.jpg', 'Author5', 'Publisher5', 'Fiction', 'Humor', 'Haha!', 5, '25.00', '2018-03-21 00:13:07'),
(6, 'Book6', 'book6.jpg', 'Author6', 'Publisher6', 'Fiction', 'Crime/Detective', '?', 6, '36.00', '2018-03-21 00:22:08'),
(7, 'Book7', 'book7.jpg', 'Author7', 'Publisher7', 'Fiction', 'Picture book', 'Doodles', 7, '49.00', '2018-03-21 03:45:43'),
(8, 'Book8', 'book8.jpg', 'Author8', 'Publisher8', 'Fiction', 'Fantasy', 'Zap!', 8, '64.00', '2018-03-21 03:45:43'),
(9, 'Book9', 'book9.jpg', 'Author9', 'Publisher9', 'Fiction', 'Fantasy', 'Zoom!', 9, '9.00', '2018-03-21 03:45:44'),
(10, 'Book10', 'book10.jpg', 'Author10', 'Publisher10', 'Fiction', 'Fantasy', 'Zippaw!', 10, '1.50', '2018-03-21 03:45:44'),
(11, 'Book11', 'book11.jpg', 'Author11', 'Publisher11', 'Fiction', 'Fantasy', 'Pew pew!', 11, '11.11', '2018-03-21 03:45:44'),
(12, 'Book12', 'book12.jpg', 'Author12', 'Publisher12', 'Fiction', 'Fantasy', 'Boom boom!', 12, '15.45', '2018-03-21 03:46:41'),
(13, 'Book13', 'book13.jpg', 'Author13', 'Publisher13', 'Fiction', 'Fantasy', 'Firebolt!', 13, '36.50', '2018-03-21 04:58:49'),
(14, 'Book14', 'book14.jpg', 'Author14', 'Publisher14', 'Fiction', 'Fantasy', 'Icebolt!', 14, '28.99', '2018-03-21 04:59:41'),
(17, 'Book15', 'book15.jpg', 'Author15', 'Publisher15', 'Fiction', 'Fantasy', 'Thunderbolt!', 5, '123.00', '2018-03-23 04:56:09');

-- --------------------------------------------------------

--
-- Table structure for table `main_genre`
--

CREATE TABLE `main_genre` (
  `genreName` varchar(20) NOT NULL,
  `mainGenreDesc` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_genre`
--

INSERT INTO `main_genre` (`genreName`, `mainGenreDesc`) VALUES
('Fiction', 'Nonfactual descriptions and events invented by the author'),
('Non-Fiction', 'A communication in which descriptions and events are understood to be factual');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `orderIDN` int(11) UNSIGNED NOT NULL,
  `customerIDN` int(11) NOT NULL,
  `productIDN` int(11) NOT NULL,
  `pricePer` decimal(7,2) UNSIGNED NOT NULL,
  `totalDue` decimal(7,2) UNSIGNED NOT NULL,
  `amountOrdered` int(11) UNSIGNED NOT NULL,
  `orderDate` datetime NOT NULL,
  `shippedDate` datetime NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'Pending'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sub_genre`
--

CREATE TABLE `sub_genre` (
  `subGenreName` varchar(30) NOT NULL,
  `mainGenre` varchar(20) NOT NULL,
  `subGenreDesc` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_genre`
--

INSERT INTO `sub_genre` (`subGenreName`, `mainGenre`, `subGenreDesc`) VALUES
('Biography', 'Non-Fiction', 'narrative of a person\'s life; when the author is also the subject, this is an autobiography'),
('Classic', 'Fiction', 'fiction that has become part of an accepted literary canon, widely taught in schools'),
('Comics/Graphic novel', 'Fiction', 'comic magazine or book based on a sequence of pictures (often hand drawn) and few words'),
('Crime/Detective', 'Fiction', 'fiction about a crime, how the criminal gets caught, and the repercussions of the crime'),
('Essay', 'Non-Fiction', 'short literary composition that reflects the author\'s outlook or point'),
('Fable', 'Fiction', 'legendary, supernatural tale demonstrating a useful truth'),
('Fairy tale', 'Fiction', 'story about fairies or other magical creatures'),
('Fan fiction', 'Fiction', 'fiction written by a fan of, and featuring characters from, a particular TV series, movie, or book'),
('Fantasy', 'Fiction', 'fiction with subplot(s), theme(s), major and minor characters, in which the narrative is presented in verse form (usually free verse)'),
('Folklore', 'Fiction', 'the songs, stories, myths, and proverbs of a people or \"folk\" as handed down by word of mouth'),
('Historical fiction', 'Fiction', 'story with fictional characters and events in an historical setting'),
('Horror', 'Fiction', 'fiction in which events evoke a feeling of dread and sometimes fear in both the characters and the reader'),
('Humor', 'Fiction', 'usually a fiction full of fun, fancy, and excitement, meant to entertain and sometimes cause intended laughter; but can be contained in all genres'),
('Instruction manual', 'Non-Fiction', 'an instructional book or booklet that is supplied with consumer products such as vehicles, home appliances, firearms, toys and computer peripherals'),
('Journalism', 'Non-Fiction', 'reporting on news and current events'),
('Lab Report', 'Non-Fiction', 'a report of an experiment'),
('Legend', 'Fiction', 'story, sometimes of a national or folk hero, that has a basis in fact but also includes imaginative material'),
('Magical realism', 'Fiction', 'story where magical or unreal elements play a natural part in an otherwise realistic environment'),
('Memoir', 'Non-Fiction', 'factual story that focuses on a significant relationship between the writer and a person, place, or object; reads like a short novel'),
('Meta fiction', 'Fiction', 'uses self-reference to draw attention to itself as a work of art while exposing the \"truth\" of a story'),
('Mystery', 'Fiction', 'fiction dealing with the solution of a crime or the revealing of secrets'),
('Mythology', 'Fiction', 'legend or traditional narrative, often based in part on historical events, that reveals human behavior and natural phenomena by its symbolism; often pertaining to the actions of the gods'),
('Mythopoeia', 'Fiction', 'fiction in which characters from religious mythology, traditional myths, folklore and/or history are recast into a re-imagined realm created by the author'),
('Personal Narrative', 'Non-Fiction', 'factual information about a significant event presented in a format that tells a story'),
('Picture book', 'Fiction', 'picture storybook is a book with very little words and a lot of pictures; picture stories are usually for children'),
('Realistic fiction', 'Fiction', 'story that is true to life'),
('Reference', 'Non-Fiction', 'such as a dictionary, thesaurus, encyclopedia, almanac, or atlas'),
('Science fiction', 'Fiction', 'story based on the impact of actual, imagined, or potential science, often set in the future or on other planets'),
('Self-help', 'Non-Fiction', 'information with the intention of instructing readers on solving personal problems'),
('Short story', 'Fiction', 'fiction of great brevity, usually supports no subplots'),
('Speech', 'Non-Fiction', 'public address or discourse'),
('Suspense/Thriller', 'Fiction', 'fiction about harm about to befall a person or group and the attempts made to evade the harm'),
('Tall tale', 'Fiction', 'humorous story with blatant exaggerations, such as swaggering heroes who do the impossible with nonchalance'),
('Textbook', 'Non-Fiction', 'authoritative and detailed factual description of a topic'),
('Western', 'Fiction', 'fiction set in the American Old West frontier and typically in the late eighteenth to late nineteenth century');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bibliophile`
--
ALTER TABLE `bibliophile`
  ADD PRIMARY KEY (`userIDN`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`bookIDN`),
  ADD KEY `fk_mainGenre` (`mainGenre`),
  ADD KEY `subGenre` (`subGenre`);

--
-- Indexes for table `main_genre`
--
ALTER TABLE `main_genre`
  ADD PRIMARY KEY (`genreName`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`orderIDN`),
  ADD KEY `customerIDN` (`customerIDN`),
  ADD KEY `productIDN` (`productIDN`);

--
-- Indexes for table `sub_genre`
--
ALTER TABLE `sub_genre`
  ADD PRIMARY KEY (`subGenreName`),
  ADD KEY `mainGenre` (`mainGenre`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bibliophile`
--
ALTER TABLE `bibliophile`
  MODIFY `userIDN` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `bookIDN` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `orderIDN` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `books`
--
ALTER TABLE `books`
  ADD CONSTRAINT `books_ibfk_1` FOREIGN KEY (`subGenre`) REFERENCES `sub_genre` (`subGenreName`),
  ADD CONSTRAINT `fk_mainGenre` FOREIGN KEY (`mainGenre`) REFERENCES `main_genre` (`genreName`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`customerIDN`) REFERENCES `bibliophile` (`userIDN`),
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`productIDN`) REFERENCES `books` (`bookIDN`);

--
-- Constraints for table `sub_genre`
--
ALTER TABLE `sub_genre`
  ADD CONSTRAINT `sub_genre_ibfk_1` FOREIGN KEY (`mainGenre`) REFERENCES `main_genre` (`genreName`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
