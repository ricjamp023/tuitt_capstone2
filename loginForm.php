<?php 
	session_start();
	//loginErr variable to output error messages
	$loginErr = "";

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	//initialize server user to php variables
		$servername = "localhost";
		$username = "ricjamp";
		$password = "";
		$dbname = "bookthrift";
	//get user login
		$userLogin = $_POST['username'];
		$userPass = $_POST['password'];
		try {
			//create connection
			$conn = new PDO("mysql:host=$servername;dbname=$dbname",$username,$password);
			//PDO error mode to exception
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "select * from bibliophile where userLogin = '$userLogin'";
			$stmt = $conn->prepare($sql);
			$stmt->execute();
			$stmt->setFetchMode(PDO::FETCH_ASSOC);
			//if there is data. Else display error.
			if ($row = $stmt->fetch()){
				// check if password is correct. Else display error
				if (password_verify($userPass,$row['userPass'])){
					if ($row['userCred'] === 'admin')
						header('Location: admin/adminPage.php?admin=true');
						else {
							$_SESSION['userlogged'] = $userLogin;
							$_SESSION['userEmail'] = $row['email'];
							$_SESSION['firstName'] = $row['firstName'];
							$_SESSION['lastName'] = $row['lastName'];
							$_SESSION['contactNum'] = $row['contactNum'];
							$_SESSION['addressLine'] = $row['addressLine'];
							$_SESSION['city'] = $row['city'];
							$_SESSION['province'] = $row['province'];
							$_SESSION['zip_code'] = $row['zip_code'];
							header('Location: index.php');
						}
				}
					else { 
						//for first try wrong password
						if($_COOKIE['loginCtr'] == 0) {
							$loginErr = "It seems your password is wrong.";
							setcookie("loginCtr","1",time()+300);
						}
							else
								$loginErr = "You could try the Forget Password option below.";
					}
			}
			else
				$loginErr = "Invalid login details. Would you like to register instead?";
		}
		catch(PDOException $e) {
			echo $sql . "<br>" . $e->getMessage();
			}
		// close PDO connection
		$conn = null;
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Bookthrift Log In</title>

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<style type="text/css">
		.loginCol { margin-top: 2rem; border: 2px double black; }
		form a { display:block; margin: 10px auto; text-align: center}
		.invalid-input { font-size: 80%; color: red; width: 100%; margin-top: .25rem; }
	</style>
</head>
<body>
	<div class="container-fluid">
		<!-- 3 columns - form is centered -->
		<div class="row">
			<!-- Empty Row for left space -->
			<div class="col-sm"></div>
			<div class="loginCol col">
				<h3 class="text-center">Login to your account</h3>
				<div class="invalid-input">
					<?php echo $loginErr; ?>
				</div>
				<form action='<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>' method='post'>
					<div class="form-group">
						<label for="username">Username</label>
						<input class="form-control" type="text" name="username" autofocus></input>
					</div>
					<div class="form-group">
						<label for="password">Password</label>
						<input class="form-control" type="password" name="password"></input>
					</div>
					<button class="btn" type="submit">Log In</button>
					<a href="#">Forgot your password?</a>
					<a href="registerForm.php">New User? Register here!</a>
				</form>
			</div>
			<!-- Empty Row for right space -->
			<div class="col-sm"></div>
		</div>
	</div>
</body>
</html>