<!DOCTYPE html>
<html>
<head>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<!-- Animate CSS -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans|Slabo+27px" rel="stylesheet"> 
	<style type="text/css">
		/*Test 1 - font face for wood carving fonts*/
		/*@font-face { font-family: "CuttingCorners"; src: url('resources/fonts/Cutting Corners.ttf'); }*/
		/*Test 2 - font face for wood carving fonts*/
		/*@font-face { font-family: "CameoAntique"; src: url('resources/fonts/CameoAntique.ttf'); }*/
		/*Test 3 - font face for wood carving fonts*/
		@font-face { font-family: "SancreekReg"; src: url('resources/fonts/Sancreek-Regular.ttf'); }
		html { height: 100%; }
		.shelfBG { background-image: url(images/BG/bg-darkwood.png); }
		.shelfBG * { color: #eba07c; font-family: 'Slabo 27px', serif; text-shadow: 0px 1px 0px rgba(255,255,255,.3), 0px -1px 0px rgba(0,0,0,.7); }
		#siteName { margin-bottom: 0; padding-top: 1rem; padding-bottom: 0.5rem; }
		#siteName a { font-family: "SancreekReg"; font-size: 3rem; color: #eba07c; }
		#siteName a:hover { text-decoration: none; }
		/*background courtesy of http://www.pixeden.com*/
		#bookshelf { height: 100%; border-bottom: 2rem solid transparent; border-image: url(images/BG/bg-darkwood.png) 30 round; padding-right: 0; padding-left: 0; }
		.siteMenu { border: groove #eba07c 3px; padding: 1rem; position: relative; height: 4.5rem; }
		#searchIcon * { color: black; }
		.siteMenuLink { position: absolute; left: 0; right: 0; margin: auto; font-size: 1.5rem; font-weight: 600; }
		@media (max-width: 1100px) {
			.twoLineMenuLink1 { font-size: 1rem; }
		}
		@media (min-width: 568px) and (max-width: 800px) {
			.twoLineMenuLink2 { font-size: 1rem; }
		}
		@media (max-width: 567px) {
			.twoLineMenuLink1,.twoLineMenuLink2 { font-size: 1.5rem; }
		}
		.dropdown-menu { /*Make the menu bigger*/right: 0 !important; border: 3px groove brown; }

	</style>
</head>

<body>

	<h1 id="siteName" class="shelfBG text-center"><a href="index.php">The Book Thrift</a></h1>
	<div class="shelfBG container-fluid">
		<div class="row text-center">
			<div class="siteMenu col-md col-xs-12">
				<?php 
					//check if user has logged in
					if (isset($_SESSION['userlogged']) && !empty($_SESSION['userlogged'])) {
						echo "<div class='dropdown'>
								<a class='dropdown-toggle siteMenuLink twoLineMenuLink1' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
									Hello, {$_SESSION['userlogged']}
			        			</a>
				        		<div class='dropdown-menu text-center' aria-labelledby='navbarDropdown'>
									<a class='dropdown-item' href='acctRedirect.php?active=account'>My Account</a>
				        			<a class='dropdown-item' href='acctRedirect.php?active=cart'>My Cart <span class='badge badge-pill badge-success'>4</span></a>
					            	<div class='dropdown-divider'></div>
					          		<a class='dropdown-item' href='logout.php'>Logout</a>
				        		</div>
							  </div>";
					} else {
						echo "<div class='dropdown'>
								<a class='dropdown-toggle siteMenuLink twoLineMenuLink1' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
									Account
			        			</a>
				        		<div class='dropdown-menu text-center' aria-labelledby='navbarDropdown'>
				        			<a class='dropdown-item' href='loginForm.php'>Login</a>
					            	<div class='dropdown-divider'></div>
					          		<a class='dropdown-item' href='registerForm.php'>Register</a>
				        		</div>
							  </div>";
						}
				?>
			</div>
			<div class="siteMenu col-md col-xs-12">
				<a class="siteMenuLink twoLineMenuLink2" href="#">Browse Books</a>
			</div>
			<div class="siteMenu col-md col-xs-12">
				<form>
					<div class="input-group">
						<input type="text" class="form-control" name="searchBooks" placeholder="Search">
						<div class="input-group-append">
							<button id="searchIcon" class="btn btn-info" type="submit"><i class="fas fa-search"></i></button>
						</div>
					</div>
				</form>
			</div>
			<div class="siteMenu col">
				<a class="siteMenuLink" href="#">About</a>
			</div>
			<div class="siteMenu col">
				<a class="siteMenuLink" href="#">Contact</a>
			</div>
		</div>
	</div>

	<!-- Jquery and Bootstrap JS -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<!-- Font Awesome -->
	<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>

</body>
</html>