<?php 
//initialize server user to php variables
		$servername = "localhost";
		$username = "ricjamp";
		$password = "";
		$dbname = "bookthrift";
		try {
			//create connection
			$conn = new PDO("mysql:host=$servername;dbname=$dbname",$username,$password);
			//PDO error mode to exception
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "select bookIDN,title,bookImg,author,publisher,subGenre,bookDesc,stocks,price from books";
			$stmt = $conn->prepare($sql);
			$stmt->execute();
			$stmt->setFetchMode(PDO::FETCH_ASSOC);
			echo "<table class='table text-center'>
					<thead>
						<tr id='bookTableHRow'>
							<th scope='col'>Image</th>
							<th scope='col'>Title</th>
							<th scope='col'>Author</th>
							<th scope='col'>Publisher</th>
							<th scope='col'>Main Genre</th>
							<th scope='col'>Description</th>
							<th scope='col'>Quantity</th>
							<th scope='col'>Price</th>
							<th scope='col'>Edit</th>
						</tr>
					</thead>
					<tbody>";
			//while there is data. Else display error.
			// Set a ctr to check if there is data
			$ctr = 0;
			while ($row = $stmt->fetch()){
				echo "<tr id='book{$ctr}'>
						<td style='display:none'>{$row['bookIDN']}</td>
						<td><img class='img-fluid' src='images/{$row['bookImg']}'></td>
						<td>{$row['title']}</td>
						<td>{$row['author']}</td>
						<td>{$row['publisher']}</td>
						<td>{$row['subGenre']}</td>
						<td>{$row['bookDesc']}</td>
						<td>{$row['stocks']}</td>
						<td>Php. {$row['price']}</td>
						<td><button class='editProd btn btn-primary'>Edit Product</button></td>
					</tr>";
				$ctr++;
			}
			if($ctr == 0) {
				echo "<tr><td colspan='7'>No data found</td></tr>";
			}
			//close table
			echo "</tbody></table>";
		}
		catch(PDOException $e) {
			echo $sql . "<br>" . $e->getMessage();
			}
		// close PDO connection
		$conn = null;
?>